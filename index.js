
// Get Dependencies/Modules
const express = require('express');
const mongoose = require('mongoose');
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');
const cors = require('cors');

// Server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

// Connect to MongoDB
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.qrju6.mongodb.net/courseBooking?retryWrites=true&w=majority',{
	useNewUrlParser: true,
	useUnifiedTopology: true
});


app.use('/api/users',userRoutes); 

app.use('/api/courses',courseRoutes);



// Server listening
app.listen(port,()=>console.log(`Listening to port ${port}`));