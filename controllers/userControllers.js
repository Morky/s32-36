const User = require('../models/User');
const Course = require('../models/Course');
const auth = require('../auth');
const bcrypt = require('bcrypt');


module.exports.checkEmail = (email)=>{
	return User.findOne({email: email}).then((result, error)=>{
		if (result != null) {
			return false
		} else {
			if (result == null) {
				return true
			} else {
				return error
			}
		}
	})
}


module.exports.register = (reqBody)=>{
	const {firstName, lastName, email, password, mobileNo, age} = reqBody;
	const newUser = new User({
		firstName: firstName,
		lastName: lastName,
		email: email,
		password: bcrypt.hashSync(password,10),
		mobileNo: mobileNo,
		age: age
	});

	return newUser.save().then((result, error)=>{
		if (result) {
			return result
		} else {
			return error
		}
	})
}

module.exports.getAllUsers = ()=>{
	return User.find({}).then(result=>{
		return result
	})
}

module.exports.login = (reqBody)=>{
	return User.findOne({email: reqBody.email}).then((result, error)=>{
		console.log(result);
		if (result == null) {
			console.log('email null');
			return false;
		} else {
			let isPwCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPwCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		} 
	})
}

module.exports.getUserProfile = (reqBody)=>{
	return User.findById({_id: reqBody.id}).then(result => {
		return result;
	})
}


module.exports.editProfile = (userId, reqBody)=>{
	// console.log(userId);
	// console.log(reqBody);
	return User.findByIdAndUpdate(userId, reqBody, {returnDocument:'after'}).then(result=>{
		result.password = '****';
		return result;
	})
}


module.exports.editUser = (userId, reqBody)=>{
	const updatedUser = {
		firstName: reqBody.firstName,
	    lastName: reqBody.lastName,
	    email: reqBody.email,
	    password: bcrypt.hashSync(reqBody.password,10),
	    mobileNo: reqBody.mobileNo,
	    age: reqBody.age
	}
	return User.findByIdAndUpdate(userId, updatedUser, {returnDocument: 'after'}).then(result=>{
		result.password = '****'
		return result;
	})
}


module.exports.editDetails = (userEmail, reqBody)=>{
	// console.log(userEmail)
	const editedUser = {
		firstName: reqBody.firstName,
	    lastName: reqBody.lastName,
	    email: reqBody.email,
	    password: bcrypt.hashSync(reqBody.password,10),
	    mobileNo: reqBody.mobileNo,
	    age: reqBody.age
	}
	return User.findOneAndUpdate(userEmail, editedUser, {returnDocument: 'after'}).then(result =>{
		result.password = '****';
		return result;
		
	})
}


module.exports.delete = (userId)=>{
	return User.findByIdAndDelete(userId).then(result => {
		return true;
	})
}


module.exports.deleteUserProfile = (userEmail)=>{
	return User.findOneAndDelete(userEmail).then(result => {
		return true;
	})
}

module.exports.enroll = async (data)=>{
	const {userId, courseId} = data;
	const userEnroll = await User.findById(userId).then((result, err)=>{
		if (err) {
			return err;
		} else {
			result.enrollments.push({courseId: courseId});
			return result.save().then(result => true)
		}
	})	
	// look for matching document of a user
	const courseEnroll = await Course.findById(courseId).then((result, err)=>{
		if (err) {
			return err;
		} else {
			result.enrollees.push({userId: userId});
			return result.save().then(result => {
				return true;
			})
		}
	})
	// to return only one value for the funtion enroll
	if (userEnroll && courseEnroll) {
		return true;
	} else {
		return false;
	}
}