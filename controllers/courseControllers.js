const Course = require('../models/Course');
const auth = require('../auth');
const bcrypt = require('bcrypt');


module.exports.getAllCourses = ()=>{
	return Course.find({}).then((result,error)=>{
		if (result) {
			return result;
		} else {
			return error;
		}
	})
}


module.exports.createCourse = (userId, reqBody)=>{
	let newCourse = new Course({
		courseName: reqBody.courseName,
		description: reqBody.description,
		price: reqBody.price,
		isActive: reqBody.isActive,
		enrollees: [{
			userId: userId,
		}]
	})
	return newCourse.save().then((result,error)=>{
		if (result) {
			return result;
		} else {
			return error;
		}
	})
}


module.exports.getActiveCourses = ()=>{
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}


module.exports.getSpecificCourse = (reqBody)=>{
	// console.log(reqBody);
	return Course.findOne({courseName: reqBody}).then((result, error) => {
		if (result == null) {
			return `Course not existing`
		} else {
			if (result) {
				return result
			} else {
				return error
			}
		}
	})
}


module.exports.getCourseById = (courseId)=>{
	return Course.findById(courseId).then(result => {
		return result;
	})
}


module.exports.archiveCourse = (courseName)=>{
	return Course.findOneAndUpdate(courseName, {isActive: false},{returnDocument: 'after'}).then(result => {
		return result;
	})
}