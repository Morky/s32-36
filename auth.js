const jwt = require('jsonwebtoken');
const secret = "MagandaAko";

module.exports.createAccessToken = (user)=>{
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret, {});
}


module.exports.decodeAccessToken = (token)=>{
	// console.log(token);
	let slicedToken = token.slice(7, token.length);
	// console.log(slicedToken);

	return jwt.decode(slicedToken);
}


module.exports.verifyToken = (req, res, next)=>{
	let token = req.headers.authorization;
	// console.log(token);
	

	if (typeof token !== "undefined") {
		let slicedToken = token.slice(7, token.length);
		return jwt.verify(slicedToken, secret, (err,data)=>{
			if (err) {
				res.send({auth: 'failed'})
			} else {
				next();
			}
		})
	} else {
		res.send(false);
	}
}