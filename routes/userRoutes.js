const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers')
const auth = require('./../auth');



router.get('/', (req, res)=>{
	userController.getAllUsers().then(result => res.send(result));
})

router.post('/email-exists',(req, res)=>{
	userController.checkEmail(req.body.email).then(result => res.send(result));
});

router.post('/register',(req, res)=>{
	userController.register(req.body).then(result => res.send(result));
})

router.post('/login', (req, res)=>{
	userController.login(req.body).then(result => res.send(result));
})

router.get('/profile', auth.verifyToken, (req, res)=>{
	// console.log(req.headers.authorization);
	let userData = auth.decodeAccessToken(req.headers.authorization);
	userController.getUserProfile(userData).then(result => res.send(result));
})

router.put('/:userId/edit',(req, res)=>{
	// console.log(req.params)
	// console.log(req.body);
	const userId = req.params.userId;
	userController.editProfile(userId,req.body).then(result => res.send(result));
})


router.put('/edit', auth.verifyToken, (req, res)=>{
	let payload = auth.decodeAccessToken(req.headers.authorization);
	// console.log(payload.id);
	userController.editUser(payload.id, req.body).then(result=>res.send(result));
})


// MINI ACTIVITY - create a route to update user details with the following
// endpoint = '/edit-profile'
// function name: editDetails
// method: PUT
// use email as filter to findOne() method

router.put('/edit-profile', auth.verifyToken, (req, res)=>{
	let payload = auth.decodeAccessToken(req.headers.authorization);
	// console.log(payload.email);
	userController.editDetails(payload.email, req.body).then(result=>res.send(result));
})



//mini activity
	// create a route to delete a specific user with the following:
		// endpoint = "/:userId/delete"
		// function name: delete()
		// method: delete
		// use id as filter to findOneAndDelete method
		// return true if the document was successfully deleted


router.delete('/:userId/delete', (req, res)=>{
	userController.delete(req.params.id).then(result => res.send(result));
})


router.delete('/delete-profile', auth.verifyToken, (req,res)=>{
	let payload = auth.decodeAccessToken(req.headers.authorization);
	// console.log(payload.email);
	userController.deleteUserProfile(payload.email).then(result => res.send(result));
})


router.post('/enroll', auth.verifyToken, (req, res)=>{
	let data = {
		userId: auth.decodeAccessToken(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(result => res.send(result));
})



module.exports = router;