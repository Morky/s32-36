const express = require('express');
const router = express.Router();
const courseController = require("./../controllers/courseControllers")
const auth = require("./../auth");


  
//create a course
router.post("/create-course", auth.verifyToken, (req, res) => {
	let payload = auth.decodeAccessToken(req.headers.authorization);
	courseController.createCourse(payload.id, req.body).then(result => res.send(result));
});


//retrieve all courses
router.get("/", (req, res) => {
	courseController.getAllCourses().then(result => res.send(result))
});


//retrieving only active courses
router.get("/active-courses", auth.verifyToken, (req, res) => {
	courseController.getActiveCourses().then(result => res.send(result))
})

//get a specific course using findOne()
router.get("/specific-course", auth.verifyToken, (req, res) => {
	courseController.getSpecificCourse(req.body.courseName).then(result => res.send(result));
})


router.get('/:courseId', auth.verifyToken, (req, res)=>{
	let paramsId = req.params.courseId;
	courseController.getCourseById(paramsId).then(result => res.send(result));
})


// update isActive status of the course using findOneAndUpdate()
// update isActive status to false
router.put('/archive', auth.verifyToken, (req, res)=>{
	courseController.archiveCourse(req.body.courseName).then(result => res.send(result));
})



module.exports = router;
